#!/bin/bash
set -e
cd "$(dirname "${0}")"/..

echo "Pretend the tests ran successfully at $(date)"

exit 0
