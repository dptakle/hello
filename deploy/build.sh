#!/bin/bash
set -e
cd "$(dirname "${0}")"/..

BUILD_LOG=build_log.txt

date | tee ${BUILD_LOG}
echo "Current directory: $(pwd)" | tee -a ${BUILD_LOG}
env | tee -a ${BUILD_LOG}

gcc -o hello hello.c 2>&1 | tee -a ${BUILD_LOG}
