#!/bin/bash
#set -e
cd "$(dirname "${0}")"/..

BUILD_LOG=build_log.txt

if [ -f "hello" ]; then
    ssh-add -l | tee -a ${BUILD_LOG} 
    ssh-add -L | tee -a ${BUILD_LOG} 
    scp -v \
        -o UserKnownHostsFile=/dev/null \
        -o StrictHostKeyChecking=no \
        hello duket@www.mknote.us:hello | tee -a ${BUILD_LOG}
else
    echo "error: missing the file" | tee -a ${BUILD_LOG}
    exit 2
fi

exit 0
